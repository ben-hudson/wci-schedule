package com.wcischeduleapp.hudson;

import org.json.JSONObject;

public interface AsyncTaskListener {

	void responseTrigger(JSONObject jObj);

}
