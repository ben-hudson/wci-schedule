package com.wcischeduleapp.hudson;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends Activity implements AsyncTaskListener {

	private Handler handler = null;
	private boolean run = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		handler = new Handler();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPause() {
		super.onPause();
		run = false;
	}

	@Override
	protected void onResume() {
		super.onResume();
		new GetJSONTask(this).execute();
		run = true;
	}

	@Override
	public void responseTrigger(JSONObject jObj) {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(this);
		TextView dateLabel = (TextView) findViewById(R.id.date_label);
		TextView messageLabel = (TextView) findViewById(R.id.message_label);
		ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);

		boolean success = false;
		String date = "Uh oh!";
		String message = "Something terrible has happened!";

		if (jObj != null) {
			try {
				success = jObj.getBoolean("success");
				if (success) {
					date = jObj.getString("date");
					message = jObj.getString("message0");
				}
			} catch (JSONException e) {
				Log.e("JSON Exception", e.toString());
			}

			try {
				message += settings.getString(jObj.getString("period0"),
						jObj.getString("period0"));
				message += jObj.getString("message1");
				message += settings.getString(jObj.getString("period1"),
						jObj.getString("period1"));
			} catch (JSONException e) {
				Log.e("JSON Exception", e.toString());
			}
		} else {
			message = "Looks like you aren't connected to the internet… please connect and try again!";
		}

		dateLabel.setText(date);
		messageLabel.setText(message);

		progressBar.setVisibility(View.INVISIBLE);
		dateLabel.setVisibility(View.VISIBLE);
		messageLabel.setVisibility(View.VISIBLE);

		if (run) {
			handler.postDelayed(new AsyncTaskDispatcher(this), 60000);
		}
	}

}
