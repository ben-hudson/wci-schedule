package com.wcischeduleapp.hudson;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.EditText;

public class SettingsActivity extends Activity {

	private SharedPreferences settings;
	private EditText fields[] = new EditText[10];
	private String keys[] = { "period A", "period B", "period C", "period D",
			"period E", "period F", "period G", "period H", "period I",
			"period J" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		setupActionBar();

		fields[0] = (EditText) findViewById(R.id.a_field);
		fields[1] = (EditText) findViewById(R.id.b_field);
		fields[2] = (EditText) findViewById(R.id.c_field);
		fields[3] = (EditText) findViewById(R.id.d_field);
		fields[4] = (EditText) findViewById(R.id.e_field);
		fields[5] = (EditText) findViewById(R.id.f_field);
		fields[6] = (EditText) findViewById(R.id.g_field);
		fields[7] = (EditText) findViewById(R.id.h_field);
		fields[8] = (EditText) findViewById(R.id.i_field);
		fields[9] = (EditText) findViewById(R.id.j_field);

		settings = PreferenceManager.getDefaultSharedPreferences(this);
		for (int i = 0; i < 10; i++) {
			fields[i].setText(settings.getString(keys[i], ""));
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStop() {
		super.onStop();

		SharedPreferences.Editor editor = settings.edit();
		for (int i = 0; i < 10; i++) {
			if (!fields[i].getText().toString().matches("")) {
				editor.putString(keys[i], fields[i].getText().toString().trim());
			}
		}
		editor.commit();
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

}
