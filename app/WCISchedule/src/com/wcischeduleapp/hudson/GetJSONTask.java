package com.wcischeduleapp.hudson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class GetJSONTask extends AsyncTask<String, String, JSONObject> {

	private AsyncTaskListener listener = null;

	public GetJSONTask(AsyncTaskListener listener) {
		super();
		this.listener = listener;
		Log.d("GetJSONTask", "Running task");
	}

	@Override
	protected JSONObject doInBackground(String... arg) {
		InputStream stream = null;
		String json = "";
		JSONObject jObj = null;

		try {
			URLConnection url = new URL("http://wcischeduleapp.com/app/get.php")
					.openConnection();
			stream = url.getInputStream();
		} catch (MalformedURLException e) {
			Log.e("Malformed URL Exception", e.toString());
		} catch (IOException e) {
			Log.e("IO Exception", e.toString());
		} catch (NullPointerException e) {
			Log.e("Null Pointer Exception", e.toString());
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					stream, "ISO-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			stream.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Exception", e.toString());
		}

		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("JSON Exception", e.toString());
		}

		return jObj;
	}

	@Override
	protected void onPostExecute(JSONObject result) {
		listener.responseTrigger(result);
	}

}
