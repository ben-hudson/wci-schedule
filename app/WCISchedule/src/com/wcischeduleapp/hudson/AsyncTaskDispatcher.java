package com.wcischeduleapp.hudson;

public class AsyncTaskDispatcher implements Runnable {

	private AsyncTaskListener listener = null;

	public AsyncTaskDispatcher(AsyncTaskListener listener) {
		super();
		this.listener = listener;
	}

	@Override
	public void run() {
		new GetJSONTask(listener).execute();
	}

}
