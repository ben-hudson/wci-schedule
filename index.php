<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="/favicon.png" type="image/png">
        <link rel="shortcut icon" type="image/png" href="http://www.wcischeduleapp.com/app/favicon.png" />
        <title>WCI Schedule</title>
        <script type="text/javascript">
            setTimeout(function() {location = 'http://wcischeduleapp.com/app/index.php'}, 60000)
        </script>
    </head>
    <body style="font-family: sans-serif; text-align:center">
        <img style="display:block; margin-left:auto; margin-right:auto; width:12%; height:12%" src="http://wcischeduleapp.com/app/icon.png"></img>
        <?php
        function format_time($minutes)
        {
            if ($minutes >= 60) {
                $hours = floor($minutes / 60);
                $minutes -= $hours * 60;
                
                if ($hours > 1) {
                    if ($minutes == 0) {
                        return $hours . " hours";
                    } else if ($minutes == 1) {
                        return $hours . " hours and 1 minute";
                    }
                    return $hours . " hours and " . $minutes . " minutes";
                } else {
                    if ($minutes == 0) {
                        return "1 hour";
                    } else if ($minutes == 1) {
                        return "1 hour and 1 minute";
                    }
                    return "1 hour and " . $minutes . " minutes";
                }
            } else {
                if ($minutes > 1) {
                    return $minutes . " minutes";
                }
                return "1 minute";
            }
        }

        $link = mysqli_connect("servername", "user", "password", "database");
        $success = true;

        if (mysqli_connect_errno()) {
            $success = false;
        }

        if ($result = mysqli_query($link, "SELECT * FROM header ORDER BY id LIMIT 1")) {
            while ($row = mysqli_fetch_row($result)) {
                $starts = strtotime($row[3]);
                $ends = strtotime($row[4]);
            }
            mysqli_free_result($result);
        } else {
            $success = false;
        }

        $today = time();

        $days = floor(($today - $starts) / (60 * 60 * 24));
        $weeks = floor($days / 7);
        $school_day = $days - $weeks * 2;

        if ($result = mysqli_query($link, "SELECT * FROM holidays")) {
            while ($row = mysqli_fetch_row($result)) {
                $type = $row[1];
                $count = (bool) $row[2];
                $date = strtotime($row[3]);
                
                if ($today < $date) {
                    break;
                }
                if ($today >= $date && $today < $date + 24 * 60 * 60) {
                    $holiday = true;
                    break;
                }
                if ($today > $date) {
                    $school_day--;
                }
            }
            mysqli_free_result($result);
        } else {
            $success = false;
        }

        $day_of_week = date("w", $today);
        if ($day_of_week == 0) {
            $school_day--;
        } else if ($day_of_week == 1) {
            $school_day -= 2;
        }

        $classes = array(
            array("A", "B", "E", "F", "I", "J"),
            array("C", "D", "E", "F", "G", "H"),
            array("A", "B", "C", "H", "I", "J"),
            array("A", "D", "E", "F", "J", "G"),
            array("B", "D", "C", "H", "J", "I")
        );

        if ($success) {
            if ($today > $starts && $today < $ends && !$holiday && $day_of_week != 6 && $day_of_week != 0) {
                echo date("l F jS g:i A", $today) . " (Day " . ($school_day % 5 + 1) . ") <br /><br />";
                
                $minutes = date("i", $today) + date("G", $today) * 60;
                $next_period = 8 * 60 + 14;
                
                if ($minutes < $next_period) {
                    $minutes_left = $next_period - $minutes;
                    echo format_time($minutes_left) . " left before the beginning of period " . $classes[$school_day % 5][0];
                } else if ($minutes > 14 * 60 + 40) {
                    echo "School is over for today<br />Next day is Day " . (($school_day + 1) % 5 + 1) . ", first period is period " . $classes[($school_day + 1) % 5][0];
                } else {
                    for ($i = 0; $i < 6; $i++) {
                        $next_period += 61;
                        if ($minutes < $next_period) {
                            $minutes_left = $next_period - $minutes;
                            if ($i == 5) {
                                echo format_time($minutes_left) . " left in period " . $classes[$school_day % 5][$i] . "<br />Next day is Day " . (($school_day + 1) % 5 + 1) . ", first period is period " . $classes[($school_day + 1) % 5][0];
                            } else {
                                echo format_time($minutes_left) . " left in period " . $classes[$school_day % 5][$i] . "<br />Next period is period " . $classes[$school_day % 5][$i + 1];
                            }
                            break;
                        }
                        $next_period += 4;
                        if ($minutes < $next_period) {
                            $minutes_left = $next_period - $minutes;
                            echo format_time($minutes_left) . " left before the beginning of period " . $classes[$school_day % 5][$i + 1];
                            break;
                        }
                    }
                }
            } else {
                echo date("l F jS g:i A", $today) . "<br /><br />";
                if ($today < $starts) {
                    echo "The school year hasn't started yet&hellip;";
                } else if ($today > $ends) {
                    echo "The school year is over&hellip;";
                } else if ($day_of_week == 6 || $day_of_week == 0) {
                    echo "No normal school today<br />Next day is Day " . ($school_day % 5 + 1) . ", first period is period " . $classes[$school_day % 5][0];
                } else if ($holiday) {
                    echo "No normal school today (" . $type . ")<br />Next day is Day " . ($school_day % 5 + 1) . ", first period is period " . $classes[$school_day % 5][0];
                } else {
                    echo "Sorry! It looks like something went wrong&hellip;";
                }
            }
        } else {
            echo "Sorry! It looks like something went wrong&hellip;";
        }

        mysqli_close($link);
        ?>
        <br />
        <br />
        <div style="font-size:xx-small">The WCI Schedule app is open source! You can check out its source code <a href="https://bitbucket.org/ben-hudson/wci-schedule/overview">here</a>.</div>
    </body>
</html>