-- phpMyAdmin SQL Dump
-- version 2.8.0.1
-- http://www.phpmyadmin.net
-- 
-- Generation Time: Sep 04, 2013 at 03:04 PM
-- Server version: 5.0.91
-- PHP Version: 4.4.9
-- 
-- Database: `wcischeduleapp`
-- 
CREATE DATABASE `wcischeduleapp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `wcischeduleapp`;

-- --------------------------------------------------------

-- 
-- Table structure for table `header`
-- 

CREATE TABLE `header` (
  `id` int(11) NOT NULL auto_increment,
  `year` varchar(10) NOT NULL,
  `school_days` int(11) NOT NULL,
  `starts` date NOT NULL,
  `ends` date NOT NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2015 DEFAULT CHARSET=latin1 AUTO_INCREMENT=2015 ;

-- 
-- Dumping data for table `header`
-- 

INSERT INTO `header` VALUES (2014, '2013/2014', 189, '2013-09-03', '2014-06-25', '2013-08-29 17:19:38');

-- --------------------------------------------------------

-- 
-- Table structure for table `holidays`
-- 

CREATE TABLE `holidays` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(50) NOT NULL,
  `count` tinyint(1) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

-- 
-- Dumping data for table `holidays`
-- 

INSERT INTO `holidays` VALUES (1, 'First Day of School', 1, '2013-09-03');
INSERT INTO `holidays` VALUES (2, 'PA Day', 0, '2013-10-11');
INSERT INTO `holidays` VALUES (3, 'Thanksgiving Day', 0, '2013-10-14');
INSERT INTO `holidays` VALUES (4, 'PA Day', 0, '2013-12-20');
INSERT INTO `holidays` VALUES (5, 'Winter Break', 0, '2013-12-23');
INSERT INTO `holidays` VALUES (6, 'Winter Break', 0, '2013-12-24');
INSERT INTO `holidays` VALUES (7, 'Winter Break', 0, '2013-12-25');
INSERT INTO `holidays` VALUES (8, 'Winter Break', 0, '2013-12-26');
INSERT INTO `holidays` VALUES (9, 'Winter Break', 0, '2013-12-27');
INSERT INTO `holidays` VALUES (10, 'Winter Break', 0, '2013-12-30');
INSERT INTO `holidays` VALUES (11, 'Winter Break', 0, '2013-12-31');
INSERT INTO `holidays` VALUES (12, 'Winter Break', 0, '2014-01-01');
INSERT INTO `holidays` VALUES (13, 'Winter Break', 0, '2014-01-02');
INSERT INTO `holidays` VALUES (14, 'Winter Break', 0, '2014-01-03');
INSERT INTO `holidays` VALUES (15, 'January Exams', 1, '2014-01-29');
INSERT INTO `holidays` VALUES (16, 'January Exams', 1, '2014-01-30');
INSERT INTO `holidays` VALUES (17, 'January Exams', 1, '2014-01-31');
INSERT INTO `holidays` VALUES (18, 'PA Day', 0, '2014-02-03');
INSERT INTO `holidays` VALUES (19, 'Family Day', 0, '2014-02-17');
INSERT INTO `holidays` VALUES (20, 'PA Day', 0, '2014-03-07');
INSERT INTO `holidays` VALUES (21, 'March Break', 0, '2014-03-10');
INSERT INTO `holidays` VALUES (22, 'March Break', 0, '2014-03-11');
INSERT INTO `holidays` VALUES (23, 'March Break', 0, '2014-03-12');
INSERT INTO `holidays` VALUES (24, 'March Break', 0, '2014-03-13');
INSERT INTO `holidays` VALUES (25, 'March Break', 0, '2014-03-14');
INSERT INTO `holidays` VALUES (26, 'Literacy Test Day', 0, '2014-03-27');
INSERT INTO `holidays` VALUES (27, 'Easter Weekend Holiday', 0, '2014-04-18');
INSERT INTO `holidays` VALUES (28, 'Easter Weekend Holiday', 0, '2014-04-21');
INSERT INTO `holidays` VALUES (29, 'Victoria Day Holiday', 0, '2014-05-19');
INSERT INTO `holidays` VALUES (30, 'Yearbook Day', 1, '2014-06-13');
INSERT INTO `holidays` VALUES (31, 'Tutorial Day', 1, '2014-06-16');
INSERT INTO `holidays` VALUES (32, 'June Exams', 1, '2014-06-17');
INSERT INTO `holidays` VALUES (33, 'June Exams', 1, '2014-06-18');
INSERT INTO `holidays` VALUES (34, 'June Exams', 1, '2014-06-19');
INSERT INTO `holidays` VALUES (35, 'June Exams', 1, '2014-06-20');
INSERT INTO `holidays` VALUES (36, 'June Exams', 1, '2014-06-23');
INSERT INTO `holidays` VALUES (37, 'June Exams', 1, '2014-06-24');
INSERT INTO `holidays` VALUES (38, 'June Exams', 1, '2014-06-25');
